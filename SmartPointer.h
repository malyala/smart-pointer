/*
Name : Smartpointer.h
Author: Amit Malyala, Copyright Amit Malyala 2019- Current.
Description:
a class

Notes:

Write a smart pointer with similar functionality as unique_ptr and use it in class X

Version: 0.1
*/
#ifndef CANONICAL_H
#define CANONICAL_H
#include <iostream>
#include "std_types.h"
#include <vector>
#include <algorithm>
#include <memory>
#include <utility>
#include <cstdlib>
/* Create Smart pointer type */
namespace  customPointers
{

template <typename T>
class my_smartptr
{
private:

    /* Number of allocations */
    std::size_t size ;
    /* Pointer*/
     T *ptr;
public:
    my_smartptr() : size(0), ptr(nullptr) {	}
    my_smartptr(T* ptr) : size(1), ptr(ptr) {}
    my_smartptr(T* ptr, std::size_t capacity) : size(capacity), ptr(ptr) {}
    my_smartptr(nullptr_t val)
    {
    	ptr=val;
    	size=0;
	}
    

    void erase(void)
    {
        if (size)
        {
            delete[] ptr;
            ptr=nullptr;
        }
       
       
        size=0;
    }
    ~my_smartptr()
    {
        if (size)
        {
            delete[] ptr;
            ptr=nullptr;
        }
        
        
    }    
    /* Move constructor */
	my_smartptr (my_smartptr&& right )
    {
    	size=right.size;
        ptr=std::move(right.ptr);
        right.ptr=nullptr;
        
	}
    my_smartptr (const my_smartptr& right )
    {
    	size=right.size;
    	
    	//Allocate memory here 
    	ptr = new T[size];
    	*ptr=*(right.ptr); 
	}
    


	
	
	
	T& operator* ()
	{
		return *ptr;
	}
	
	T& operator[] (std::size_t index)
	{
		return ptr[index];
	}
	
	
	const T& operator* () const
	{
		return *ptr;
	}
	
	T* operator->()  
	{ 
	   return ptr; 
	}
	
    T const * operator->() const 
	{ 
	   return ptr;
	}
	/* for printing address of ptr */
    friend std::ostream& operator<<(std::ostream& os, const my_smartptr<T>& obj)
    {
        os << *obj;
        return os;
    }
};

/* make_unique example . We will use this type of allocator with variadic template */
template<typename T, typename... Args>
my_smartptr<T> MemAllocator(Args&&... args)
{
    return customPointers::my_smartptr<T> (new T(std::forward<Args>(args)...));
}



} // namespace  customPointers


#endif 