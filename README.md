# Smart Pointer

This project creates a smart pointer of any type in C++. The pointer functions similar to std::unqiue_ptr although it doesn't have all of its
features. A function similar to std::make_unique is also implemented. More features are to be added to this.